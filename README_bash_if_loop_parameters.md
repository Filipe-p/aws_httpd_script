# Bash scripting 102

This readme is to document what we learn around:

- `if` conditions
- looping
- parameters

All in bash.

## If conditions Bash

The `if` condition is a function that evaluates a statement/condition.
If this condition returns true, then a block of code runs.
Else it moves to the next statement.

```
#syntax
if <condition>; then <command>fi
# example
if [ -r somefile ]
then
     content=$(cat somefile)
elif [ -f somefile ]
then
     echo "the file 'somefile' exists but is not readable to the script."
else
     echo "the file 'somefile' does not exist."
fi
```

- the above example shows format things to take note of are spacing at the start and end of `[]`  e.g. `[-r somefile]` will return an error regarding missing `]` instead you want `[ -r somefile ]`
- you can use `-a`(and) and `-o`(or) within `[]` to combine conditions


## Parameters 

Parameters in bash are defined in files or cammand using $n.

Example:

```bash
#syntax
$n

#example calling a parameter
echo $1 $2
```

These can passed by calling a file/program and following with the necessary parameters using spaces to delimiter the next parameter. 

example:
```bash

#file_example.sh
#!/bin/bash
echo $1 $2 $1 $1

# calling the file

$ ./file_example.sh arg1 agr2

> arg1 agr2 arg1 arg1
```

## Loops 