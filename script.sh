#!/bin/bash
# Tell the shell what type of file it is. Not needed in earlier versions of linux

#Use if condition with params to make this take in a ip. 
REMOTE_BANANAS=$1
ARG_TWO=$2

echo $REMOTE_BANANAS
echo $ARG_TWO
# connects to the remote machine and then run the command inside the '' -> in the remote machine
ssh -o "StrictHostKeyChecking=no" -i ~/.ssh/cohort8filipessh.pem ec2-user@$REMOTE_BANANAS '

#Installing apache http server
sudo yum -y install httpd

# Starting apache service
# sudo systemctl start httpd

# if condition that checks if httpd is installed.
  # then start the service 
# else 
  # BREAK my script --- I want to know this failed.

if rpm -qa | grep "^httpd-[0-9]"
then
    sudo systemctl start httpd
else
    # exit 0 is success, exit 1 is general errors, exit 2 Misuse of shell builtins 
    exit 1
fi



# sudo sh -c  opens a bash session in sudo - this is a sudo redirect 
# with cat we define the input in the << _END _END_
# Output of cat is contactinated (>) into /var/www/html/index.html
sudo sh -c "cat >/var/www/html/index.html << _END_
<h1>
YELLOWwww 😆
</h1>
<p> this is a paragraph </p>
_END_"
'